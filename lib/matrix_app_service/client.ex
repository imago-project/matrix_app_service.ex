defmodule MatrixAppService.Client do
  def client(user_id \\ nil, storage \\ nil) do
    base_url = Application.get_env(:matrix_app_service, :base_url)
    access_token = Application.get_env(:matrix_app_service, :access_token)

    %Polyjuice.Client{
      base_url: base_url,
      access_token: access_token,
      # "@alice:matrix.imago.local",
      user_id: user_id,
      storage: storage
    }
  end

  def create_room(options) do
    client_with_options(options)
    |> Polyjuice.Client.Room.create_room(options)
  end

  def create_alias(room_id, room_alias, options \\ []) do
    client_with_options(options)
    |> Polyjuice.Client.Room.create_alias(room_id, room_alias)
  end

  defp client_with_options(options) do
    user_id = Keyword.get(options, :user_id, nil)
    storage = Keyword.get(options, :storage, nil)
    client(user_id, storage)
  end
end
