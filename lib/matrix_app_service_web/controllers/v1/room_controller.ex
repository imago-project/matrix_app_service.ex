defmodule MatrixAppServiceWeb.V1.RoomController do
  use MatrixAppServiceWeb, :controller

  def show(conn, %{"room_alias" => room_alias}) do
    module = Application.fetch_env!(:matrix_app_service, :room_module)

    with :ok <- module.query_alias(room_alias) do
      send_resp(conn, 200, "{}")
    else
      _ ->
        send_resp(conn, 404, "")
    end
  end
end
